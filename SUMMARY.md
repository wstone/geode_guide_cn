# Summary

-----
* [前言](README.md)

-----
* [Apache Geode入门](Geode_1_Getting_Started_with_Apache_Geode.md)
  * [关于 Apache](Geode_1_Getting_Started_with_Apache_Geode.md#关于Apache)
  * [Apache Geode的主要特点](Geode_1_Getting_Started_with_Apache_Geode.md#ApacheGeode的主要特点)
  * [先决条件和安装说明](Geode_1_Getting_Started_with_Apache_Geode.md#先决条件和安装说明)
    * [主机要求](Geode_1_Getting_Started_with_Apache_Geode.md#主机要求)
    * [如何安装](Geode_1_Getting_Started_with_Apache_Geode.md#如何安装)
    * [设置CLASSPATH](Geode_1_Getting_Started_with_Apache_Geode.md#设置CLASSPATH)
    * [如何卸载](Geode_1_Getting_Started_with_Apache_Geode.md#如何卸载)
  * [Apach Geode在15分钟或更短时间内完成](Geode_1_Getting_Started_with_Apache_Geode.md#ApacheGeode在15分钟或更短时间内完成)

-----
* [配置和运行集群](Geode_2_Configuring_and_Running_a_Cluster.md)
  * [集群配置服务概述](Geode_2_Configuring_and_Running_a_Cluster.md#集群配置服务概述)
  * [教程 - 创建和使用集群配置](Geode_2_Configuring_and_Running_a_Cluster.md#教程创建和使用集群配置)
  * [将应用程序JAR部署到Apache Geode成员](Geode_2_Configuring_and_Running_a_Cluster.md#将应用程序JAR部署到ApacheGeode成员)
  * [使用成员组](Geode_2_Configuring_and_Running_a_Cluster.md#使用成员组)
  * [导出和导入集群配置](Geode_2_Configuring_and_Running_a_Cluster.md#导出和导入集群配置)
  * [集群配置文件和故障排除](Geode_2_Configuring_and_Running_a_Cluster.md#集群配置文件和故障排除)
  * [使用gfsh通过HTTP或HTTPS管理远程集群](Geode_2_Configuring_and_Running_a_Cluster.md#使用gfsh通过HTTP或HTTPS管理远程集群)
  * [在没有集群配置服务的情况下部署配置文件](Geode_2_Configuring_and_Running_a_Cluster.md#在没有集群配置服务的情况下部署配置文件)
    * [部署配置文件的主要步骤](Geode_2_Configuring_and_Running_a_Cluster.md#部署配置文件的主要步骤)
    * [默认文件规范和搜索位置](Geode_2_Configuring_and_Running_a_Cluster.md#默认文件规范和搜索位置)
    * [更改文件规范](Geode_2_Configuring_and_Running_a_Cluster.md#更改文件规范)
    * [部署配置JAR的示例](Geode_2_Configuring_and_Running_a_Cluster.md#部署配置JAR的示例)
  * [启动和关闭系统](Geode_2_Configuring_and_Running_a_Cluster.md#启动和关闭系统)
  * [运行Geode定位器进程](Geode_2_Configuring_and_Running_a_Cluster.md#运行Geode定位器进程)
  * [运行Geode服务器进程](Geode_2_Configuring_and_Running_a_Cluster.md#运行Geode服务器进程)
  * [管理系统输出文件](Geode_2_Configuring_and_Running_a_Cluster.md#管理系统输出文件)
  * [防火墙注意事项](Geode_2_Configuring_and_Running_a_Cluster.md#防火墙注意事项)
    * [防火墙和连接](Geode_2_Configuring_and_Running_a_Cluster.md#防火墙和连接)
    * [防火墙和端口](Geode_2_Configuring_and_Running_a_Cluster.md#防火墙和端口)

-----
* [基本配置和编程](Geode_3_Basic_Configuration_and_Programming.md)
  * [集群和缓存配置](Geode_3_Basic_Configuration_and_Programming.md#集群和缓存配置)
    * [集群成员](Geode_3_Basic_Configuration_and_Programming.md#集群成员)
    * [设置属性](Geode_3_Basic_Configuration_and_Programming.md#设置属性)
    * [配置缓存和数据区域的选项](Geode_3_Basic_Configuration_and_Programming.md#配置缓存和数据区域的选项)
    * [本地和远程成员身份和缓存](Geode_3_Basic_Configuration_and_Programming.md#本地和远程成员身份和缓存)
  * [缓存管理](Geode_3_Basic_Configuration_and_Programming.md#缓存管理)
    * [缓存管理简介](Geode_3_Basic_Configuration_and_Programming.md#缓存管理简介)
    * [管理对等或服务器缓存](Geode_3_Basic_Configuration_and_Programming.md#管理对等或服务器缓存)
    * [管理客户端缓存](Geode_3_Basic_Configuration_and_Programming.md#管理客户端缓存)
    * [管理安全系统中的缓存](Geode_3_Basic_Configuration_and_Programming.md#管理安全系统中的缓存)
    * [管理多个安全用户的RegionServices](Geode_3_Basic_Configuration_and_Programming.md#管理多个安全用户的RegionServices)
    * [初始化缓存后启动应用程序](Geode_3_Basic_Configuration_and_Programming.md#初始化缓存后启动应用程序)
  * [数据区域](Geode_3_Basic_Configuration_and_Programming.md#数据区域)
    * [区域管理](Geode_3_Basic_Configuration_and_Programming.md#区域管理)
    * [区域命名](Geode_3_Basic_Configuration_and_Programming.md#区域命名)
    * [区域快捷方式和自定义命名区域属性](Geode_3_Basic_Configuration_and_Programming.md#区域快捷方式和自定义命名区域属性)
    * [存储和检索区域快捷方式和自定义命名区域属性](Geode_3_Basic_Configuration_and_Programming.md#存储和检索区域快捷方式和自定义命名区域属性)
    * [管理区域属性](Geode_3_Basic_Configuration_and_Programming.md#管理区域属性)
    * [为区域和条目创建自定义属性](Geode_3_Basic_Configuration_and_Programming.md#为区域和条目创建自定义属性)
    * [用现有内容构建新区域](Geode_3_Basic_Configuration_and_Programming.md#用现有内容构建新区域)
  * [数据条目](Geode_3_Basic_Configuration_and_Programming.md#数据条目)
    * [管理数据条目](Geode_3_Basic_Configuration_and_Programming.md#管理数据条目)
    * [在数据缓存中使用自定义类的要求](Geode_3_Basic_Configuration_and_Programming.md#在数据缓存中使用自定义类的要求)

-----
* [拓扑和通信](Geode_4_Topologies_and_Communication.md)
  * [拓扑和通信一般概念](Geode_4_Topologies_and_Communication.md#拓扑和通信一般概念)
    * [拓扑类型](Geode_4_Topologies_and_Communication.md#拓扑类型)
    * [规划拓扑和通信](Geode_4_Topologies_and_Communication.md#规划拓扑和通信)
    * [成员发现如何运作](Geode_4_Topologies_and_Communication.md#成员发现如何运作)
    * [通信如何工作](Geode_4_Topologies_and_Communication.md#通信如何工作)
    * [使用绑定地址](Geode_4_Topologies_and_Communication.md#使用绑定地址)
    * [在IPv4和IPv6之间进行选择](Geode_4_Topologies_and_Communication.md#在IPv4和IPv6之间进行选择)
  * [点对点配置](Geode_4_Topologies_and_Communication.md#点对点配置)
    * [配置Peer-to-Peer(点对点)发现](Geode_4_Topologies_and_Communication.md#配置PeertoPeer点对点发现)
    * [配置对等通信](Geode_4_Topologies_and_Communication.md#配置对等通信)
    * [将Peer(同行)组织成逻辑成员组](Geode_4_Topologies_and_Communication.md#将Peer同行组织成逻辑成员组)
  * [客户端/服务器配置](Geode_4_Topologies_and_Communication.md#客户端服务器配置)
    * [标准客户端/服务器部署](Geode_4_Topologies_and_Communication.md#标准客户端服务器部署)
    * [服务器发现如何工作](Geode_4_Topologies_and_Communication.md#服务器发现如何工作)
    * [客户端/服务器连接如何工作](Geode_4_Topologies_and_Communication.md#客户端服务器连接如何工作)
    * [配置客户端/服务器系统](Geode_4_Topologies_and_Communication.md#配置客户端服务器系统)
    * [将服务器组织到逻辑成员组中](Geode_4_Topologies_and_Communication.md#将服务器组织到逻辑成员组中)
    * [客户端/服务器示例配置](Geode_4_Topologies_and_Communication.md#客户端服务器示例配置)
    * [微调您的客户端/服务器配置](Geode_4_Topologies_and_Communication.md#微调您的客户端服务器配置)
  * [多站点(WAN)配置](Geode_4_Topologies_and_Communication.md#多站点WAN配置)
    * [多站点(WAN)系统的工作原理](Geode_4_Topologies_and_Communication.md#多站点WAN系统的工作原理)
      * [多站点缓存概述](Geode_4_Topologies_and_Communication.md#多站点缓存概述)
      * [WAN更新的一致性](Geode_4_Topologies_and_Communication.md#WAN更新的一致性)
      * [多站点系统的发现](Geode_4_Topologies_and_Communication.md#多站点系统的发现)
      * [网关发件人](Geode_4_Topologies_and_Communication.md#网关发件人)
      * [网关接收器](Geode_4_Topologies_and_Communication.md#网关接收器)
    * [多站点(WAN)拓扑](Geode_4_Topologies_and_Communication.md#多站点WAN拓扑)
    * [配置多站点(WAN)系统](Geode_4_Topologies_and_Communication.md#配置多站点WAN系统)
    * [过滤多站点(WAN)分发的事件](Geode_4_Topologies_and_Communication.md#过滤多站点WAN分发的事件)
    * [解决冲突事件](Geode_4_Topologies_and_Communication.md#解决冲突事件)

-----
* [管理 Apache Geode](Geode_5_Managing_Apache_Geode.md)
  * [Apache Geode管理和监控](Geode_5_Managing_Apache_Geode.md#ApacheGeode管理和监控)
    * [管理和监控功能](Geode_5_Managing_Apache_Geode.md#管理和监控功能)
    * [Geode管理和监控工具概述](Geode_5_Managing_Apache_Geode.md#Geode管理和监控工具概述)
    * [架构和组件](Geode_5_Managing_Apache_Geode.md#架构和组件)
    * [JMX管理器操作](Geode_5_Managing_Apache_Geode.md#JMX管理器操作)
      * [启动一个 JMX Manager](Geode_5_Managing_Apache_Geode.md#启动一个JMXManager)
      * [配置一个 JMX Manager](Geode_5_Managing_Apache_Geode.md#配置一个JMXManager)
      * [停止一个 JMX管理器](Geode_5_Managing_Apache_Geode.md#停止一个JMX管理器)
    * [联邦MBean架构](Geode_5_Managing_Apache_Geode.md#联邦MBean架构)
      * [Geode JMX MBean列表](Geode_5_Managing_Apache_Geode.md#GeodeJMXMBean列表)
        * [JMX Manager MBeans](Geode_5_Managing_Apache_Geode.md#JMXManagerMBeans)
        * [受管节点MBean](Geode_5_Managing_Apache_Geode.md#受管节点MBean)
      * [通过JConsole浏览Geode MBean](Geode_5_Managing_Apache_Geode.md#通过JConsole浏览Geode MBean)
      * [Geode JMX MBean通知](Geode_5_Managing_Apache_Geode.md#Geode JMX MBean通知)
        * [通知联邦](Geode_5_Managing_Apache_Geode.md#通知联邦)
        * [JMX MBean通知列表](Geode_5_Managing_Apache_Geode.md#JMX MBean通知列表)
    * [配置RMI注册表端口和RMI连接器](Geode_5_Managing_Apache_Geode.md#配置RMI注册表端口和RMI连接器)
    * [通过Management API执行gfsh命令](Geode_5_Managing_Apache_Geode.md#通过Management API执行gfsh命令)
  * [管理堆和堆外内存](Geode_5_Managing_Apache_Geode.md#管理堆和堆外内存)
    * [调整JVM垃圾收集参数](Geode_5_Managing_Apache_Geode.md#调整JVM垃圾收集参数)
    * [使用Geode资源管理器](Geode_5_Managing_Apache_Geode.md#使用Geode资源管理器)
    * [使用资源管理器控制堆使用](Geode_5_Managing_Apache_Geode.md#使用资源管理器控制堆使用)
    * [配置堆用于LRU管理的Geode](Geode_5_Managing_Apache_Geode.md#配置堆用于LRU管理的Geode)
    * [设置JVM GC调整参数](Geode_5_Managing_Apache_Geode.md#设置JVMGC调整参数)
    * [监视和调整堆LRU配置](Geode_5_Managing_Apache_Geode.md#监视和调整堆LRU配置)
    * [资源管理器示例配置](Geode_5_Managing_Apache_Geode.md#资源管理器示例配置)
    * [管理 Off-Heap 内存](Geode_5_Managing_Apache_Geode.md#管理OffHeap内存)
    * [锁定内存(仅限Linux系统)](Geode_5_Managing_Apache_Geode.md#锁定内存仅限Linux系统)
  * [磁盘存储](Geode_5_Managing_Apache_Geode.md#磁盘存储)
    * [磁盘存储的工作原理](Geode_5_Managing_Apache_Geode.md#磁盘存储的工作原理)
    * [磁盘存储文件名和扩展名](Geode_5_Managing_Apache_Geode.md#磁盘存储文件名和扩展名)
    * [磁盘存储操作日志](Geode_5_Managing_Apache_Geode.md#磁盘存储操作日志)
    * [配置磁盘存储](Geode_5_Managing_Apache_Geode.md#配置磁盘存储)
      * [设计和配置磁盘存储](Geode_5_Managing_Apache_Geode.md#设计和配置磁盘存储)
      * [磁盘存储配置参数](Geode_5_Managing_Apache_Geode.md#磁盘存储配置参数)
      * [修改默认磁盘存储](Geode_5_Managing_Apache_Geode.md#修改默认磁盘存储)
    * [使用磁盘存储优化系统](Geode_5_Managing_Apache_Geode.md#使用磁盘存储优化系统)
    * [启动并关闭磁盘存储](Geode_5_Managing_Apache_Geode.md#启动并关闭磁盘存储)
    * [磁盘存储管理](Geode_5_Managing_Apache_Geode.md#磁盘存储管理)
      * [磁盘存储管理命令和操作](Geode_5_Managing_Apache_Geode.md#磁盘存储管理命令和操作)
      * [验证磁盘存储](Geode_5_Managing_Apache_Geode.md#验证磁盘存储)
      * [在磁盘存储日志文件上运行压缩](Geode_5_Managing_Apache_Geode.md#在磁盘存储日志文件上运行压缩)
      * [保持磁盘存储与缓存同步](Geode_5_Managing_Apache_Geode.md#保持磁盘存储与缓存同步)
      * [配置磁盘可用空间监视](Geode_5_Managing_Apache_Geode.md#配置磁盘可用空间监视)
      * [处理丢失的磁盘存储](Geode_5_Managing_Apache_Geode.md#处理丢失的磁盘存储)
      * [当缓冲区刷新到磁盘时更改](Geode_5_Managing_Apache_Geode.md#当缓冲区刷新到磁盘时更改)
    * [为系统恢复和运营管理创建备份](Geode_5_Managing_Apache_Geode.md#为系统恢复和运营管理创建备份)
  * [缓存和区域快照](Geode_5_Managing_Apache_Geode.md#缓存和区域快照)
    * [用法和性能说明](Geode_5_Managing_Apache_Geode.md#用法和性能说明)
    * [导出缓存和区域快照](Geode_5_Managing_Apache_Geode.md#导出缓存和区域快照)
    * [导入缓存和区域快照](Geode_5_Managing_Apache_Geode.md#导入缓存和区域快照)
    * [导入或导出期间过滤条目](Geode_5_Managing_Apache_Geode.md#导入或导出期间过滤条目)
    * [以编程方式读取快照](Geode_5_Managing_Apache_Geode.md#以编程方式读取快照)
  * [区域压缩](Geode_5_Managing_Apache_Geode.md#区域压缩)
    * [怎样得到压缩](Geode_5_Managing_Apache_Geode.md#怎样得到压缩)
    * [如何在区域中启用压缩](Geode_5_Managing_Apache_Geode.md#如何在区域中启用压缩)
    * [使用压缩器](Geode_5_Managing_Apache_Geode.md#使用压缩器)
    * [压缩和非压缩区域的性能比较](Geode_5_Managing_Apache_Geode.md#压缩和非压缩区域的性能比较)
  * [网络分区](Geode_5_Managing_Apache_Geode.md#网络分区)
    * [网络分区管理的工作原理](Geode_5_Managing_Apache_Geode.md#网络分区管理的工作原理)
    * [故障检测和成员资格视图](Geode_5_Managing_Apache_Geode.md#故障检测和成员资格视图)
    * [成员协调员，主要成员和成员加权](Geode_5_Managing_Apache_Geode.md#成员协调员，主要成员和成员加权)
    * [网络分区方案](Geode_5_Managing_Apache_Geode.md#网络分区方案)
    * [配置Apache Geode处理网络分区](Geode_5_Managing_Apache_Geode.md#配置ApacheGeode处理网络分区)
    * [防止网络分区](Geode_5_Managing_Apache_Geode.md#防止网络分区)
  * [安全](Geode_5_Managing_Apache_Geode.md#安全)
    * [安全实施简介和概述](Geode_5_Managing_Apache_Geode.md#安全实施简介和概述)
    * [安全细节考虑因素](Geode_5_Managing_Apache_Geode.md#安全细节考虑因素)
      * [外部接口，端口和服务](Geode_5_Managing_Apache_Geode.md#外部接口端口和服务)
      * [必须受到保护的资源](Geode_5_Managing_Apache_Geode.md#必须受到保护的资源)
      * [日志文件位置](Geode_5_Managing_Apache_Geode.md#日志文件位置)
      * [放置安全配置设置的位置](Geode_5_Managing_Apache_Geode.md#放置安全配置设置的位置)
    * [使用属性定义启用安全性](Geode_5_Managing_Apache_Geode.md#使用属性定义启用安全性)
    * [认证](Geode_5_Managing_Apache_Geode.md#认证)
      * [实施身份验证](Geode_5_Managing_Apache_Geode.md#实施身份验证)
      * [验证示例](Geode_5_Managing_Apache_Geode.md#验证示例)
    * [授权](Geode_5_Managing_Apache_Geode.md#授权)
      * [实施授权](Geode_5_Managing_Apache_Geode.md#实施授权)
      * [授权示例](Geode_5_Managing_Apache_Geode.md#授权示例)
    * [区域数据的后处理](Geode_5_Managing_Apache_Geode.md#区域数据的后处理)
    * [SSL](Geode_5_Managing_Apache_Geode.md#SSL)
      * [配置SSL](Geode_5_Managing_Apache_Geode.md#配置SSL)
        * [SSL可配置组件](Geode_5_Managing_Apache_Geode.md#SSL可配置组件)
        * [SSL配置属性](Geode_5_Managing_Apache_Geode.md#SSL配置属性)
        * [SSL属性参考表](Geode_5_Managing_Apache_Geode.md#SSL属性参考表)
        * [程序](Geode_5_Managing_Apache_Geode.md#程序)
      * [SSL示例实施](Geode_5_Managing_Apache_Geode.md#SSL示例实施)
  * [性能调整和配置](Geode_5_Managing_Apache_Geode.md#性能调整和配置)
    * [禁用TCP SYN Cookies](Geode_5_Managing_Apache_Geode.md#禁用TCPSYNCookies)
    * [提高vSphere的性能](Geode_5_Managing_Apache_Geode.md#提高vSphere的性能)
    * [性能控制](Geode_5_Managing_Apache_Geode.md#性能控制)
      * [数据序列化](Geode_5_Managing_Apache_Geode.md#数据序列化)
      * [设置缓存超时](Geode_5_Managing_Apache_Geode.md#设置缓存超时)
      * [控制套接字使用](Geode_5_Managing_Apache_Geode.md#控制套接字使用)
      * [慢速接收器的管理](Geode_5_Managing_Apache_Geode.md#慢速接收器的管理)
      * [增加缓存命中率](Geode_5_Managing_Apache_Geode.md#增加缓存命中率)
    * [系统成员性能](Geode_5_Managing_Apache_Geode.md#系统成员性能)
      * [成员属性](Geode_5_Managing_Apache_Geode.md#成员属性)
      * [JVM内存设置和系统性能](Geode_5_Managing_Apache_Geode.md#JVM内存设置和系统性能)
      * [垃圾收集和系统性](Geode_5_Managing_Apache_Geode.md#垃圾收集和系统性)
      * [连接线程设置和性能](Geode_5_Managing_Apache_Geode.md#连接线程设置和性能)
    * [带有TCP/IP的慢速接收器](Geode_5_Managing_Apache_Geode.md#带有TCPIP的慢速接收器)
      * [防止慢速接收器](Geode_5_Managing_Apache_Geode.md#防止慢速接收器)
      * [管理慢速接收器](Geode_5_Managing_Apache_Geode.md#管理慢速接收器)
    * [慢分布式确认消息](Geode_5_Managing_Apache_Geode.md#慢分布式确认消息)
    * [套接字通信](Geode_5_Managing_Apache_Geode.md#套接字通信)
      * [设置套接字缓冲区大小](Geode_5_Managing_Apache_Geode.md#设置套接字缓冲区大小)
      * [短暂的TCP端口限制](Geode_5_Managing_Apache_Geode.md#短暂的TCP端口限制)
      * [确保你有足够的Sockets](Geode_5_Managing_Apache_Geode.md#确保你有足够的Sockets)
      * [TCP/IP KeepAlive配置](Geode_5_Managing_Apache_Geode.md#TCPIPKeepAlive配置)
      * [TCP/IP Peer-to-Peer握手超时](Geode_5_Managing_Apache_Geode.md#TCPIPPeertoPeer握手超时)
      * [在多站点（WAN）部署中配置套接字](Geode_5_Managing_Apache_Geode.md#在多站点WAN部署中配置套接字)
    * [UDP 通信](Geode_5_Managing_Apache_Geode.md#UDP通信)
    * [组播通信](Geode_5_Managing_Apache_Geode.md#组播通信)
      * [为多播提供带宽](Geode_5_Managing_Apache_Geode.md#为多播提供带宽)
      * [测试多播速度限制](Geode_5_Managing_Apache_Geode.md#测试多播速度限制)
      * [配置多播速度限制](Geode_5_Managing_Apache_Geode.md#配置多播速度限制)
      * [多播的运行时注意事](Geode_5_Managing_Apache_Geode.md#多播的运行时注意事)
      * [排除多播调整过程](Geode_5_Managing_Apache_Geode.md#排除多播调整过程)
    * [维护缓存一致性](Geode_5_Managing_Apache_Geode.md#维护缓存一致性)
  * [日志](Geode_5_Managing_Apache_Geode.md#日志)
    * [Geode日志如何工作](Geode_5_Managing_Apache_Geode.md#Geode日志如何工作)
    * [了解日志消息及其类别](Geode_5_Managing_Apache_Geode.md#了解日志消息及其类别)
    * [命名，搜索和创建日志文件](Geode_5_Managing_Apache_Geode.md#命名搜索和创建日志文件)
    * [设置日志记录](Geode_5_Managing_Apache_Geode.md#设置日志记录)
    * [高级用户 - 为Geode配置Log4j 2](Geode_5_Managing_Apache_Geode.md#高级用户为Geode配置Log4j2)
  * [统计](Geode_5_Managing_Apache_Geode.md#统计)
    * [统计如何运作](Geode_5_Managing_Apache_Geode.md#统计如何运作)
    * [瞬态区域和条目统计](Geode_5_Managing_Apache_Geode.md#瞬态区域和条目统计)
    * [应用程序定义和自定义统计](Geode_5_Managing_Apache_Geode.md#应用程序定义和自定义统计)
    * [配置和使用统计信息](Geode_5_Managing_Apache_Geode.md#配置和使用统计信息)
    * [查看存档统计信息](Geode_5_Managing_Apache_Geode.md#查看存档统计信息)
  * [故障排除和系统恢复](Geode_5_Managing_Apache_Geode.md#故障排除和系统恢复)
    * [生成用于故障排除的工件](Geode_5_Managing_Apache_Geode.md#生成用于故障排除的工件)
    * [诊断系统问题](Geode_5_Managing_Apache_Geode.md#诊断系统问题)
    * [系统故障和恢复](Geode_5_Managing_Apache_Geode.md#系统故障和恢复)
    * [使用自动重新连接处理强制缓存断开连接](Geode_5_Managing_Apache_Geode.md#使用自动重新连接处理强制缓存断开连接)
    * [从应用程序和缓存服务器崩溃中恢复](Geode_5_Managing_Apache_Geode.md#从应用程序和缓存服务器崩溃中恢复)
      * [使用点对点配置从崩溃中恢复](Geode_5_Managing_Apache_Geode.md#使用点对点配置从崩溃中恢复)
      * [使用客户端/服务器配置从崩溃中恢](Geode_5_Managing_Apache_Geode.md#使用客户端/服务器配置从崩溃中恢)
    * [从机器崩溃中恢复](Geode_5_Managing_Apache_Geode.md#从机器崩溃中恢复)
    * [从ConfictingPersistentDataExceptions中恢复](Geode_5_Managing_Apache_Geode.md#从ConfictingPersistentDataExceptions中恢复)
    * [防止和恢复磁盘完全错误](Geode_5_Managing_Apache_Geode.md#防止和恢复磁盘完全错误)
    * [理解和恢复网络中断](Geode_5_Managing_Apache_Geode.md#理解和恢复网络中断)

-----
* [使用Apache Geode进行开发](Geode_6_Developing_with_Apache_Geode.md)
  * [区域数据存储和分发](Geode_6_Developing_with_Apache_Geode.md#区域数据存储和分发)
    * [存储和分配选项](Geode_6_Developing_with_Apache_Geode.md#存储和分配选项)
      * [点对点区域存储和分发](Geode_6_Developing_with_Apache_Geode.md#点对点区域存储和分发)
    * [区域类型](Geode_6_Developing_with_Apache_Geode.md#区域类型)
      * [复制区域](Geode_6_Developing_with_Apache_Geode.md#复制区域)
      * [分布式，非复制区域](Geode_6_Developing_with_Apache_Geode.md#分布式非复制区域)
      * [本地区域](Geode_6_Developing_with_Apache_Geode.md#本地区域)
    * [区域数据存储和数据访问器](Geode_6_Developing_with_Apache_Geode.md#区域数据存储和数据访问器)
    * [动态创建区域](Geode_6_Developing_with_Apache_Geode.md#动态创建区域)
  * [分区区域](Geode_6_Developing_with_Apache_Geode.md#分区区域)
    * [了解分区](Geode_6_Developing_with_Apache_Geode.md#了解分区)
      * [数据分区](Geode_6_Developing_with_Apache_Geode.md#数据分区)
      * [分区区域操作](Geode_6_Developing_with_Apache_Geode.md#分区区域操作)
      * [有关分区区域的其他信息](Geode_6_Developing_with_Apache_Geode.md#有关分区区域的其他信息)
    * [配置分区区域](Geode_6_Developing_with_Apache_Geode.md#配置分区区域)
    * [配置分区区域的桶数](Geode_6_Developing_with_Apache_Geode.md#配置分区区域的桶数)
      * [计算分区区域的桶总数](Geode_6_Developing_with_Apache_Geode.md#计算分区区域的桶总数)
    * [自定义分区和共享数据](Geode_6_Developing_with_Apache_Geode.md#自定义分区和共享数据)
      * [了解自定义分区和数据同地](Geode_6_Developing_with_Apache_Geode.md#了解自定义分区和数据同地)
        * [自定义分区](Geode_6_Developing_with_Apache_Geode.md#自定义分区)
        * [区域之间的数据同地](Geode_6_Developing_with_Apache_Geode.md#区域之间的数据同地)
      * [标准自定义分区](Geode_6_Developing_with_Apache_Geode.md#标准自定义分区)
      * [固定的自定义分区](Geode_6_Developing_with_Apache_Geode.md#固定的自定义分区)
      * [共享来自不同分区区域的数据](Geode_6_Developing_with_Apache_Geode.md#共享来自不同分区区域的数据)
    * [配置分区区域的高可用性](Geode_6_Developing_with_Apache_Geode.md#配置分区区域的高可用性)
      * [了解分区区域的高可用性](Geode_6_Developing_with_Apache_Geode.md#了解分区区域的高可用性)
        * [控制你的初级和二级居住地](Geode_6_Developing_with_Apache_Geode.md#控制你的初级和二级居住地)
        * [在虚拟机中运行进程](Geode_6_Developing_with_Apache_Geode.md#在虚拟机中运行进程)
        * [在高可用分区区域中进行读](Geode_6_Developing_with_Apache_Geode.md#在高可用分区区域中进行读)
      * [配置分区区域的高可用性](Geode_6_Developing_with_Apache_Geode.md#配置分区区域的高可用性)
        * [设置冗余副本数](Geode_6_Developing_with_Apache_Geode.md#设置冗余副本数)
        * [为成员配置冗余区域](Geode_6_Developing_with_Apache_Geode.md#为成员配置冗余区域)
        * [设置强制唯一主机](Geode_6_Developing_with_Apache_Geode.md#设置强制唯一主机)
        * [为分区区域配置成员崩溃冗余恢复](Geode_6_Developing_with_Apache_Geode.md#为分区区域配置成员崩溃冗余恢复)
        * [为分区区域配置成员加入冗余恢复](Geode_6_Developing_with_Apache_Geode.md#为分区区域配置成员加入冗余恢复)
    * [配置对服务器分区区域的单跳客户端访问](Geode_6_Developing_with_Apache_Geode.md#配置对服务器分区区域的单跳客户端访问)
      * [了解客户端对服务器分区区域的单跳访问](Geode_6_Developing_with_Apache_Geode.md#了解客户端对服务器分区区域的单跳访问)
        * [单跳和池最大连接设置](Geode_6_Developing_with_Apache_Geode.md#单跳和池最大连接设置)
        * [平衡单跳服务器连接使用](Geode_6_Developing_with_Apache_Geode.md#平衡单跳服务器连接使用)
      * [配置客户端对服务器分区区域的单跳访问](Geode_6_Developing_with_Apache_Geode.md#配置客户端对服务器分区区域的单跳访问)
      * [重新平衡分区区域数据](Geode_6_Developing_with_Apache_Geode.md#重新平衡分区区域数据)
        * [分区区域重新平衡的工作原理](Geode_6_Developing_with_Apache_Geode.md#分区区域重新平衡的工作原理)
        * [何时重新平衡分区区域](Geode_6_Developing_with_Apache_Geode.md#何时重新平衡分区区域)
        * [如何模拟区域重新平衡](Geode_6_Developing_with_Apache_Geode.md#如何模拟区域重新平衡)
        * [自动重新平衡](Geode_6_Developing_with_Apache_Geode.md#自动重新平衡)
      * [检查分区中的冗余](Geode_6_Developing_with_Apache_Geode.md#检查分区中的冗余)
      * [将分区区域数据移动到另一个成员](Geode_6_Developing_with_Apache_Geode.md#将分区区域数据移动到另一个成员)
  * [分布式和复制区域](Geode_6_Developing_with_Apache_Geode.md#分布式和复制区域)
    * [分布式如何运作](Geode_6_Developing_with_Apache_Geode.md#分布式如何运作)
    * [区域分布式选项](Geode_6_Developing_with_Apache_Geode.md#区域分布式选项)
    * [复制和预加载的工作原理](Geode_6_Developing_with_Apache_Geode.md#复制和预加载的工作原理)
      * [复制和预加载区域的初始化](Geode_6_Developing_with_Apache_Geode.md#复制和预加载区域的初始化)
      * [初始化后复制和预加载区域的行为](Geode_6_Developing_with_Apache_Geode.md#初始化后复制和预加载区域的行为)
    * [配置分布式，复制和预加载区域](Geode_6_Developing_with_Apache_Geode.md#配置分布式，复制和预加载区域)
      * [复制区域中的本地销毁和无效](Geode_6_Developing_with_Apache_Geode.md#复制区域中的本地销毁和无效)
    * [锁定全局区域](Geode_6_Developing_with_Apache_Geode.md#锁定全局区域)
      * [锁定超时](Geode_6_Developing_with_Apache_Geode.md#锁定超时)
      * [优化锁定性能](Geode_6_Developing_with_Apache_Geode.md#优化锁定性能)
      * [例子](Geode_6_Developing_with_Apache_Geode.md#例子)
  * [区域更新的一致性](Geode_6_Developing_with_Apache_Geode.md#区域更新的一致性)
    * [按地区类型检查一致性](Geode_6_Developing_with_Apache_Geode.md#按地区类型检查一致性)
    * [配置一致性检查](Geode_6_Developing_with_Apache_Geode.md#配置一致性检查)
    * [一致性检查的开销](Geode_6_Developing_with_Apache_Geode.md#一致性检查的开销)
    * [一致性检查如何适用于复制区域](Geode_6_Developing_with_Apache_Geode.md#一致性检查如何适用于复制区域)
    * [如何解决Destroy和Clear操作](Geode_6_Developing_with_Apache_Geode.md#如何解决Destroy和Clear操作)
    * [具有一致性区域的事务](Geode_6_Developing_with_Apache_Geode.md#具有一致性区域的事务)
  * [一般地区数据管理](Geode_6_Developing_with_Apache_Geode.md#一般地区数据管理)
    * [Persistence and Overflow（持久性和溢出）](Geode_6_Developing_with_Apache_Geode.md#PersistenceandOverflow持久性和溢出)
      * [持久性和溢出如何工作](Geode_6_Developing_with_Apache_Geode.md#持久性和溢出如何工作)
      * [配置区域持久性和溢出](Geode_6_Developing_with_Apache_Geode.md#配置区域持久性和溢出)
      * [溢出配置示例](Geode_6_Developing_with_Apache_Geode.md#溢出配置示例)
    * [Eviction（逐出）](Geode_6_Developing_with_Apache_Geode.md#Eviction逐出)
      * [驱逐如何运作](Geode_6_Developing_with_Apache_Geode.md#驱逐如何运作)
      * [配置数据驱逐](Geode_6_Developing_with_Apache_Geode.md#配置数据驱逐)
    * [Expiration（到期）](Geode_6_Developing_with_Apache_Geode.md#Expiration到期)
      * [过期如何运作](Geode_6_Developing_with_Apache_Geode.md#过期如何运作)
      * [配置数据过期](Geode_6_Developing_with_Apache_Geode.md#配置数据过期)
    * [保持缓存与外部数据源同步](Geode_6_Developing_with_Apache_Geode.md#保持缓存与外部数据源同步)
      * [外部数据源概述](Geode_6_Developing_with_Apache_Geode.md#外部数据源概述)
      * [使用JNDI配置数据库连接](Geode_6_Developing_with_Apache_Geode.md#使用JNDI配置数据库连接)
      * [数据加载器的工作原理](Geode_6_Developing_with_Apache_Geode.md#数据加载器的工作原理)
      * [实现数据加载器](Geode_6_Developing_with_Apache_Geode.md#实现数据加载器)
  * [数据序列化](Geode_6_Developing_with_Apache_Geode.md#数据序列化)
    * [数据序列化概述](Geode_6_Developing_with_Apache_Geode.md#数据序列化概述)
    * [Geode PDX序列化](Geode_6_Developing_with_Apache_Geode.md#GeodePDX序列化)
      * [Geode PDX序列化功能](Geode_6_Developing_with_Apache_Geode.md#GeodePDX序列化功能)
      * [使用PDX序列化的高级步骤](Geode_6_Developing_with_Apache_Geode.md#使用PDX序列化的高级步骤)
      * [使用基于自动反射的PDX序列化](Geode_6_Developing_with_Apache_Geode.md#使用基于自动反射的PDX序列化)
        * [使用类模式字符串自定义序列化](Geode_6_Developing_with_Apache_Geode.md#使用类模式字符串自定义序列化)
        * [扩展ReflectionBasedAutoSerializer](Geode_6_Developing_with_Apache_Geode.md#扩展ReflectionBasedAutoSerializer)
      * [使用PdxSerializer序列化您的域对象](Geode_6_Developing_with_Apache_Geode.md#使用PdxSerializer序列化您的域对象)
      * [在域对象中实现PdxSerializable](Geode_6_Developing_with_Apache_Geode.md#在域对象中实现PdxSerializable)
      * [编写应用程序以使用PdxInstances](Geode_6_Developing_with_Apache_Geode.md#编写应用程序以使用PdxInstances)
      * [将JSON文档添加到Geode缓存](Geode_6_Developing_with_Apache_Geode.md#将JSON文档添加到Geode缓存)
      * [使用PdxInstanceFactory创建PdxInstances](Geode_6_Developing_with_Apache_Geode.md#使用PdxInstanceFactory创建PdxInstances)
      * [将PDX元数据保留到磁盘](Geode_6_Developing_with_Apache_Geode.md#将PDX元数据保留到磁盘)
      * [使用PDX对象作为区域输入键](Geode_6_Developing_with_Apache_Geode.md#使用PDX对象作为区域输入键)
    * [Geode数据序列化（DataSerializable和DataSerializer）](Geode_6_Developing_with_Apache_Geode.md#Geode数据序列化DataSerializable和DataSerializer)
    * [标准Java序列化](Geode_6_Developing_with_Apache_Geode.md#标准Java序列化)
  * [事件和事件处理](Geode_6_Developing_with_Apache_Geode.md#事件和事件处理)
    * [事件是如何工作](Geode_6_Developing_with_Apache_Geode.md#事件是如何工作)
      * [点对点事件分发](Geode_6_Developing_with_Apache_Geode.md#点对点事件分发)
      * [客户端到服务器事件分发](Geode_6_Developing_with_Apache_Geode.md#客户端到服务器事件分发)
      * [多站点（WAN）事件分发](Geode_6_Developing_with_Apache_Geode.md#多站点WAN事件分发)
      * [事件处理程序和事件列表](Geode_6_Developing_with_Apache_Geode.md#事件处理程序和事件列表)
    * [实现Geode事件处理程序](Geode_6_Developing_with_Apache_Geode.md#实现Geode事件处理程序)
      * [实现缓存事件处理程序](Geode_6_Developing_with_Apache_Geode.md#实现缓存事件处理程序)
      * [为Write-Behind Cache事件处理实现AsyncEventListener](Geode_6_Developing_with_Apache_Geode.md#为WriteBehindCache事件处理实现AsyncEventListener)
      * [如何从事件处理程序回调安全地修改缓存](Geode_6_Developing_with_Apache_Geode.md#如何从事件处理程序回调安全地修改缓存)
      * [缓存事件处理程序示例](Geode_6_Developing_with_Apache_Geode.md#缓存事件处理程序示例)
    * [配置点对点事件消息](Geode_6_Developing_with_Apache_Geode.md#配置点对点事件消息)
    * [配置客户端服务器事件消息](Geode_6_Developing_with_Apache_Geode.md#配置客户端服务器事件消息)
      * [配置高可用性服务器](Geode_6_Developing_with_Apache_Geode.md#配置高可用性服务器)
        * [高度可用的客户端服务器事件消息](Geode_6_Developing_with_Apache_Geode.md#高度可用的客户端服务器事件消息)
      * [实施持久的客户端服务器消息传递](Geode_6_Developing_with_Apache_Geode.md#实施持久的客户端服务器消息传递)
      * [调整客户端服务器事件消息](Geode_6_Developing_with_Apache_Geode.md#调整客户端服务器事件消息)
        * [配置服务器订阅队列](Geode_6_Developing_with_Apache_Geode.md#配置服务器订阅队列)
        * [限制服务器的订阅队列内存使用](Geode_6_Developing_with_Apache_Geode.md#限制服务器的订阅队列内存使用)
        * [调整客户端的订阅消息跟踪超时](Geode_6_Developing_with_Apache_Geode.md#调整客户端的订阅消息跟踪超时)
    * [配置多站点（WAN）事件队列](Geode_6_Developing_with_Apache_Geode.md#配置多站点WAN事件队列)
      * [配置服务器订阅队列](Geode_6_Developing_with_Apache_Geode.md#配置服务器订阅队列)
      * [限制服务器的订阅队列内存使用](Geode_6_Developing_with_Apache_Geode.md#限制服务器的订阅队列内存使用)
      * [调整客户端的订阅消息跟踪超时](Geode_6_Developing_with_Apache_Geode.md#调整客户端的订阅消息跟踪超时)
  * [增量传播](Geode_6_Developing_with_Apache_Geode.md#增量传播)
    * [增量传播如何工作](Geode_6_Developing_with_Apache_Geode.md#增量传播如何工作)
    * [何时避免增量传播](Geode_6_Developing_with_Apache_Geode.md#何时避免增量传播)
    * [增量传播属性](Geode_6_Developing_with_Apache_Geode.md#增量传播属性)
    * [实施增量传播](Geode_6_Developing_with_Apache_Geode.md#实施增量传播)
    * [增量传播中的错误](Geode_6_Developing_with_Apache_Geode.md#增量传播中的错误)
    * [增量传播示例](Geode_6_Developing_with_Apache_Geode.md#增量传播示例)
  * [查询](Geode_6_Developing_with_Apache_Geode.md#查询)
    * [查询常见问题和示例](Geode_6_Developing_with_Apache_Geode.md#查询常见问题和示例)
    * [使用OQL查询](Geode_6_Developing_with_Apache_Geode.md#使用OQL查询)
      * [OQL的优点](Geode_6_Developing_with_Apache_Geode.md#OQL的优点)
      * [在Geode中编写和执行查询](Geode_6_Developing_with_Apache_Geode.md#在Geode中编写和执行查询)
      * [构建查询字符串](Geode_6_Developing_with_Apache_Geode.md#构建查询字符串)
        * [IMPORT Statement（IMPORT语句）](Geode_6_Developing_with_Apache_Geode.md#IMPORTStatementIMPORT语句)
        * [FROM Clause（FROM子句）](Geode_6_Developing_with_Apache_Geode.md#FROMClauseFROM子句)
        * [WHERE Clause（WHERE子句）](Geode_6_Developing_with_Apache_Geode.md#WHEREClauseWHERE子句)
        * [SELECT Statement（SELECT语句）](Geode_6_Developing_with_Apache_Geode.md#SELECTStatementSELECT语句)
        * [OQL聚合函数](Geode_6_Developing_with_Apache_Geode.md#OQL聚合函数)
      * [OQL语法和语义](Geode_6_Developing_with_Apache_Geode.md#OQL语法和语义)
        * [支持的字符集](Geode_6_Developing_with_Apache_Geode.md#支持的字符集)
        * [支持的关键字](Geode_6_Developing_with_Apache_Geode.md#支持的关键字)
        * [区分大小写](Geode_6_Developing_with_Apache_Geode.md#区分大小写)
        * [查询字符串中的注释](Geode_6_Developing_with_Apache_Geode.md#查询字符串中的注释)
        * [查询语言语法](Geode_6_Developing_with_Apache_Geode.md#查询语言语法)
        * [操作符](Geode_6_Developing_with_Apache_Geode.md#操作符)
        * [保留字](Geode_6_Developing_with_Apache_Geode.md#保留字)
        * [支持文字](Geode_6_Developing_with_Apache_Geode.md#支持文字)
      * [查询语言限制和不受支持的特性](Geode_6_Developing_with_Apache_Geode.md#查询语言限制和不受支持的特性)
    * [高级查询](Geode_6_Developing_with_Apache_Geode.md#高级查询)
      * [性能考虑](Geode_6_Developing_with_Apache_Geode.md#性能考虑)
      * [查询时监视内存不足](Geode_6_Developing_with_Apache_Geode.md#查询时监视内存不足)
      * [长时间运行查询的超时](Geode_6_Developing_with_Apache_Geode.md#长时间运行查询的超时)
      * [使用查询绑定参数](Geode_6_Developing_with_Apache_Geode.md#使用查询绑定参数)
      * [查询分区的区域](Geode_6_Developing_with_Apache_Geode.md#查询分区的区域)
        * [对分区区域使用ORDER BY](Geode_6_Developing_with_Apache_Geode.md#对分区区域使用ORDERBY)
        * [在单个节点上查询分区区域](Geode_6_Developing_with_Apache_Geode.md#在单个节点上查询分区区域)
        * [优化按键或字段值分区的数据查询](Geode_6_Developing_with_Apache_Geode.md#优化按键或字段值分区的数据查询)
        * [对分区区域执行等连接查询](Geode_6_Developing_with_Apache_Geode.md#对分区区域执行等连接查询)
        * [分区区域查询限制](Geode_6_Developing_with_Apache_Geode.md#分区区域查询限制)
      * [查询调试](Geode_6_Developing_with_Apache_Geode.md#查询调试)
    * [使用索引](Geode_6_Developing_with_Apache_Geode.md#使用索引)
      * [使用索引的提示和指南](Geode_6_Developing_with_Apache_Geode.md#使用索引的提示和指南)
      * [创建、列出和删除索引](Geode_6_Developing_with_Apache_Geode.md#创建列出和删除索引)
      * [创建键索引](Geode_6_Developing_with_Apache_Geode.md#创建键索引)
      * [创建哈希索引](Geode_6_Developing_with_Apache_Geode.md#创建哈希索引)
      * [在映射字段上创建索引（映射索引）](Geode_6_Developing_with_Apache_Geode.md#在映射字段上创建索引映射索引)
      * [一次创建多个索引](Geode_6_Developing_with_Apache_Geode.md#一次创建多个索引)
      * [维护索引（同步或异步）和索引存储](Geode_6_Developing_with_Apache_Geode.md#维护索引同步或异步和索引存储)
      * [使用查询索引提示](Geode_6_Developing_with_Apache_Geode.md#使用查询索引提示)
      * [在单个区域查询上使用索引](Geode_6_Developing_with_Apache_Geode.md#在单个区域查询上使用索引)
      * [使用带有等连接查询的索引](Geode_6_Developing_with_Apache_Geode.md#使用带有等连接查询的索引)
      * [使用带有溢出区域的索引](Geode_6_Developing_with_Apache_Geode.md#使用带有溢出区域的索引)
      * [在使用多个区域的等连接查询上使用索引](Geode_6_Developing_with_Apache_Geode.md#在使用多个区域的等连接查询上使用索引)
      * [索引例子](Geode_6_Developing_with_Apache_Geode.md#索引例子)
  * [连续查询](Geode_6_Developing_with_Apache_Geode.md#连续查询)
    * [连续查询是如何工作的](Geode_6_Developing_with_Apache_Geode.md#连续查询是如何工作的)
    * [实现连续查询](Geode_6_Developing_with_Apache_Geode.md#实现连续查询)
    * [管理连续查询](Geode_6_Developing_with_Apache_Geode.md#管理连续查询)
  * [事务](Geode_6_Developing_with_Apache_Geode.md#事务)
    * [遵守ACID语义](Geode_6_Developing_with_Apache_Geode.md#遵守ACID语义)
    * [代码示例](Geode_6_Developing_with_Apache_Geode.md#代码示例)
    * [设计注意事项](Geode_6_Developing_with_Apache_Geode.md#设计注意事项)
  * [函数执行](Geode_6_Developing_with_Apache_Geode.md#函数执行)
    * [函数执行如何工作](Geode_6_Developing_with_Apache_Geode.md#函数执行如何工作)
    * [在Apache Geode中执行一个函数](Geode_6_Developing_with_Apache_Geode.md#在ApacheGeode中执行一个函数)

